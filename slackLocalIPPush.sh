#!/bin/bash

# Bash script to get the LOCAL IP address

# Changing directory to where the script is located
cd "$(dirname "$0")";

fileLocalIP="myLocalIP.txt";
today=$(date +"%d/%m-%Y %H:%M:%S");
currentIP=$(hostname -I);
oldIP="";

# Retrieving the old IP
if [[ -f $fileLocalIP ]]
then
	# Use this if the sed command below doesn't work
	#oldIP=$(cat $file | awk "$1 == \"New IP:\" { print $2 }");

	oldIP=$(sed -n 's/New IP: //p' $fileLocalIP);
fi

# If no previous IP has been registered
if [[ ! $oldIP  ]]
then
	oldIP="No previous IP registered";
fi

# Pushing new IP address to Slack if the IP has been updated
if [[ ! -z $currentIP ]] && [[ $currentIP != $oldIP ]]
then
	# Using cURL to push message to Slack using Webhook API
	curl -s \
	-X POST \
	--data-urlencode 'payload={"username":"Raspberry Pi ('"$(hostname)"')","icon_emoji":":raspberry:","text":"My local IP is now '"$currentIP"'"}' \
	https://hooks.slack.com/services/XXXXXX/YYYYYY/ZZZZZZ > /dev/null; # PLEASE UPDATE THIS LINK WITH THE CORRECT SLACK WEBHOOK URL

	# Keeps track of the previous IP, current IP and when it was last updated
        echo "Previous IP Address: $oldIP" > $fileLocalIP;
        echo "New IP: $currentIP" >> $fileLocalIP;
        echo "Last updated: $today" >> $fileLocalIP;
fi


# If you want to see the last time it was checked, uncomment the lines below
#fileLastCheckedLocalIP="lastCheckedLocalIP.txt";
#echo "Previous IP Address: $oldIP" > $fileLastCheckedLocalIP;
#echo "IP: $currentIP" >> $fileLastCheckedLocalIP;
#echo "Last checked: $today" >> $fileLastCheckedLocalIP;
