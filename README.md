# README #


# What is this repository for? #

Setting up automatic push of local and external IP addresses to a Slack channel

## How do I get set up? ##

Clone this repository to your computer using this command in your terminal:  
```$ git clone https://stiandalviken@bitbucket.org/stiandalviken/slack-ip-push.git```


### Make the files executable ###

Bash script for local IP push: ```$ chmod +x slackLocalIPPush.sh```

Bash script for external IP push: ```$ chmod +x slackExternalIPPush.sh```


###To check for IP changes after every reboot###

For the local IP script, write: ```$ crontab -e```
and add:  
```# Running local IP push to Slack, 30 seconds after reboot```  
```@reboot /bin/sleep 30; /home/pi/slack-ip-push/slackLocalIPPush.sh```

For the external IP script, write: ```$ crontab -e```
and add:  
```# Running external IP push to Slack, 30 seconds after reboot```  
```@reboot /bin/sleep 30; /home/pi/slack-ip-push/slackExternalIPPush.sh```


###To check for IP changes every hour###

For the local IP script, write: ```$ crontab -e```
and add:  
```0 */1 * * * /home/pi/slack-ip-push/slackLocalIPPush.sh```

For the external IP script, write: ```$ crontab -e```
and add:  
```0 */1 * * * /home/pi/slack-ip-push/slackExternalPPush.sh```